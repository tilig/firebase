// Import the functions you need from the SDKs you need
import { initializeApp } from "firebase/app";
import {
  getAuth,
  connectAuthEmulator,
  signInWithEmailAndPassword,
  signOut as firebaseSignOut,
  signInWithPopup,
  GoogleAuthProvider,
  getAdditionalUserInfo,
} from "firebase/auth";
import {
  connectFunctionsEmulator,
  getFunctions,
  httpsCallable,
} from "firebase/functions";

const firebaseConfig = {
  apiKey: "AIzaSyDZmgfXZS6Lxcrdjwlnwl55fWw1R4Mwx1g",
  authDomain: "tilig-dev.firebaseapp.com",
  databaseURL: "https://tilig-dev-default-rtdb.firebaseio.com",
  projectId: "tilig-dev",
  storageBucket: "tilig-dev.appspot.com",
  messagingSenderId: "765031685617",
  appId: "1:765031685617:web:bd37dd9e7c449cb0b5b850",
  measurementId: "G-3EFF3L06R3",
};

// Initialize Firebase
const app = initializeApp(firebaseConfig);

export const auth = getAuth(app);
connectAuthEmulator(auth, "http://localhost:9099");

const functions = getFunctions(app);
connectFunctionsEmulator(functions, "localhost", 5001);

interface SessionTokenResponse {
  token: string;
}

interface KeypairMeta {
  app_platform: string;
  app_version: string;
}

interface EncryptKeypairRequest {
  key: {
    private_key: string;
    public_key: string;
    key_type: "x25519";
  };
  meta: KeypairMeta;
}
interface EncryptKeypairResponse {
  token: string;
}

interface DecryptKeypairRequest {
  encrypted_private_key: string;
}
interface DecryptKeypairResponse {
  private_key: string;
}

export const getSessionToken = httpsCallable<void, SessionTokenResponse>(
  functions,
  "getSessionToken"
);
export const encryptKeypair = httpsCallable<
  EncryptKeypairRequest,
  EncryptKeypairResponse
>(functions, "encryptKeypair");
export const decryptKeypair = httpsCallable<
  DecryptKeypairRequest,
  DecryptKeypairResponse
>(functions, "decryptPrivateKey");

export const signIn = (email: string, password: string) => {
  return signInWithEmailAndPassword(auth, email, password);
};

export const signOut = () => firebaseSignOut(auth);

export const signInWithGoogle = async () => {
  const provider = new GoogleAuthProvider();
  provider.addScope("email profile");

  const result = await signInWithPopup(auth, provider);
  console.log(
    "Result from sign in",
    result,
    result && getAdditionalUserInfo(result)
  );
};
