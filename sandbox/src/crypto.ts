import * as sodium from "libsodium-wrappers";

export type Keypair = sodium.StringKeyPair & {
  keyType: "x25519";
};

export const generateKeypair = async () => {
  await sodium.ready;
  return sodium.crypto_box_keypair("base64") as Keypair;
};
