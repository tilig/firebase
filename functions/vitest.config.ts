import { defineConfig } from "vitest/config"
import * as path from "path"

const resolve = (p: string) => path.resolve(__dirname, p)

export default defineConfig({
  resolve: {
    alias: {
      "@": resolve("src/"),
    },
  },
  test: {
    coverage: {
      provider: "c8",
    },
    include: ["src/**/__test__/**"],
  },
})
