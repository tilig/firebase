import { CallableContext } from "firebase-functions/lib/v1/providers/https"
import { describe, expect, it } from "vitest"
import { validateAuth } from "../auth"

describe("Endpoint authentication", () => {
  const endpoint = validateAuth(() => true)

  it("throws an error without auth", () => {
    expect(
      endpoint({}, { auth: undefined } as CallableContext)
    ).rejects.toThrowError("The function must be called while authenticated.")
  })

  it("throws ans error when the email is empty", () => {
    expect(
      endpoint({}, {
        auth: { token: { email: "", email_verified: true } },
      } as CallableContext)
    ).rejects.toThrowError("The user's email address is not verified.")
  })

  it("throws an error when the email is not verified", () => {
    expect(
      endpoint({}, {
        auth: {
          token: { email: "nonempty@exampl.com", email_verified: false },
        },
      } as CallableContext)
    ).rejects.toThrowError("The user's email address is not verified.")
  })

  it("resolves if authenticated", () => {
    expect(
      endpoint({}, {
        auth: {
          token: { email: "nonempty@exampl.com", email_verified: true },
        },
      } as CallableContext)
    ).resolves.toBe(true)
  })
})
