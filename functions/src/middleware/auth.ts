import { DecodedIdToken } from "firebase-admin/auth"
import { CallableContext } from "firebase-functions/lib/common/providers/https"
import { AuthData } from "firebase-functions/lib/common/providers/tasks"
import { HttpsError } from "firebase-functions/v1/auth"

/**
 * Restricted type of DecodedIdToken that has a present email and email_verified
 */
interface VerifiedIdToken extends DecodedIdToken {
  email: string
  email_verified: true
}

/**
 * Restricted type of AuthData that has an idToken with valid email
 */
export interface VerifiedAuthData extends AuthData {
  token: VerifiedIdToken
}

/**
 * Validate that there is a user present
 * and they have a verified email
 * @param auth AuthData from context
 * @return true otherwise throws
 */
export const validAuth = (auth?: AuthData): auth is VerifiedAuthData => {
  if (!auth) {
    throw new HttpsError(
      "unauthenticated",
      "The function must be called while authenticated."
    )
  }

  const { token } = auth

  if (!token.email_verified || !token.email) {
    throw new HttpsError(
      "unauthenticated",
      "The user's email address is not verified."
    )
  }

  return true
}

type Resolver<T, R> = {
  (data: T, auth: VerifiedAuthData): R
}

type AuthenticatedResolver<T, R> = {
  (data: T, context: CallableContext): Promise<R>
}

export const validateAuth = <T, R>(
  resolver: Resolver<T, R>
): AuthenticatedResolver<T, R> => {
  return async (data: T, context: CallableContext) => {
    const { auth } = context

    /* c8 ignore start */
    if (!validAuth(auth)) {
      // This code is impossible to reach, but cleans up types considerably.
      throw new HttpsError(
        "unauthenticated",
        "Could not validate authentication"
      )
    }
    /* c8 ignore stop */

    return resolver(data, auth)
  }
}
