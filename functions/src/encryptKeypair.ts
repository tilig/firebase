import { createHash } from "node:crypto"
import { HttpsError } from "firebase-functions/v1/auth"

import { SignJWT } from "jose"
import * as z from "zod"

import { VerifiedAuthData } from "./middleware/auth"
import { encryptPrivateKey } from "./service/keys"
import { getCurrentJwtSigningKey } from "./service/jwts"

const KEYPART_BYTESLENGTH = 32
const URLSAFE_BASE64 = /[A-Z0-9_-]/i

/**
 * Validate a string is valid base64url and is 32 bytes
 * @param str Base64UR encoded key part
 */
const isKeyPart = (str: string) => {
  try {
    const decoded = Buffer.from(str, "base64url")
    return decoded.length === KEYPART_BYTESLENGTH
  } catch (e) {
    return false
  }
}

const KeyPart = z.string().regex(URLSAFE_BASE64).refine(isKeyPart)

/**
 * Asymmetric Keypair of type x25519
 */
export const KeyPair = z.object({
  private_key: KeyPart,
  public_key: KeyPart,
  key_type: z.literal("x25519"),
})
export type KeyPair = z.infer<typeof KeyPair>

/**
 * Meta data of a keypair
 */
const MetaData = z
  .object({
    app_platform: z.string(),
    app_version: z.string(),
  })
  .passthrough()
type MetaData = z.infer<typeof MetaData>

export const EncryptPrivateKeyRequest = z.object({
  key: KeyPair,
  meta: MetaData,
})
export type EncryptPrivateKeyRequest = z.infer<typeof EncryptPrivateKeyRequest>

/**
 * Encrypt a keypair for storage in Tilig API
 * @param data Keypair encryption request
 * @param auth User info
 */
export const encryptKeypair = async (
  data: EncryptPrivateKeyRequest,
  auth: VerifiedAuthData
) => {
  const request = EncryptPrivateKeyRequest.safeParse(data)

  if (!request.success) {
    throw new HttpsError(
      "invalid-argument",
      "Encryption request is malformed",
      request.error
    )
  }

  const { data: keyData } = request

  let encryptedPrivateKey: Buffer

  try {
    encryptedPrivateKey = await encryptPrivateKey(
      auth.uid,
      auth.token.email,
      keyData.key.private_key
    )
  } catch (e) {
    console.error("Error during private key encryption", e)
    throw new HttpsError("internal", "Error during private key encryption")
  }

  // We use the SHA256 hash to identify the keypair
  const hash = createHash("sha256")
  hash.update(Buffer.from(keyData.key.public_key, "base64url"))
  const kid = hash.digest("base64url")

  const token = await createKeypairJwt(auth.uid, {
    kid,
    key: {
      encrypted_private_key: encryptedPrivateKey.toString("base64url"),
      public_key: keyData.key.public_key,
      key_type: keyData.key.key_type,
    },
    meta: keyData.meta,
  })

  return { token }
}

interface KeyInfoMeta extends MetaData, Record<string, unknown> {}

export interface KeyInfo extends Record<string, unknown> {
  kid: string
  key: {
    encrypted_private_key: string
    public_key: string
    key_type: string
  }
  meta: KeyInfoMeta
}

/**
 * Create a signed JWT with encrypted key pair data
 * @param uid UID of the user
 * @param keyInfo Key information
 * @return Signed JWT
 */
const createKeypairJwt = async (uid: string, keyInfo: KeyInfo) => {
  try {
    return new SignJWT({ ...keyInfo, kind: "encrypted-keypair" })
      .setProtectedHeader({ alg: "ES256" })
      .setAudience("tilig-saas")
      .setIssuer("tilig-ks")
      .setIssuedAt()
      .setSubject(`${uid}#key-${keyInfo.kid}`)
      .sign(await getCurrentJwtSigningKey())
  } catch (e) {
    console.error("Error signing keypair JWT", e)
    throw new HttpsError(
      "unavailable",
      "Cannot create keypair tokens at this time"
    )
  }
}
