/*eslint-disable block-scoped-var, id-length, no-control-regex, no-magic-numbers, no-prototype-builtins, no-redeclare, no-shadow, no-var, sort-vars*/
"use strict";

var $protobuf = require("protobufjs/minimal");

// Common aliases
var $Reader = $protobuf.Reader, $Writer = $protobuf.Writer, $util = $protobuf.util;

// Exported root namespace
var $root = $protobuf.roots["default"] || ($protobuf.roots["default"] = {});

$root.keys = (function() {

    /**
     * Namespace keys.
     * @exports keys
     * @namespace
     */
    var keys = {};

    keys.EncryptedPrivateKey = (function() {

        /**
         * Properties of an EncryptedPrivateKey.
         * @memberof keys
         * @interface IEncryptedPrivateKey
         * @property {number|null} [version] EncryptedPrivateKey version
         * @property {Uint8Array|null} [encryptedPrivateKey] EncryptedPrivateKey encryptedPrivateKey
         * @property {string|null} [uid] EncryptedPrivateKey uid
         * @property {string|null} [email] EncryptedPrivateKey email
         * @property {number|Long|null} [issuedAt] EncryptedPrivateKey issuedAt
         */

        /**
         * Constructs a new EncryptedPrivateKey.
         * @memberof keys
         * @classdesc Represents an EncryptedPrivateKey.
         * @implements IEncryptedPrivateKey
         * @constructor
         * @param {keys.IEncryptedPrivateKey=} [properties] Properties to set
         */
        function EncryptedPrivateKey(properties) {
            if (properties)
                for (var keys = Object.keys(properties), i = 0; i < keys.length; ++i)
                    if (properties[keys[i]] != null)
                        this[keys[i]] = properties[keys[i]];
        }

        /**
         * EncryptedPrivateKey version.
         * @member {number} version
         * @memberof keys.EncryptedPrivateKey
         * @instance
         */
        EncryptedPrivateKey.prototype.version = 0;

        /**
         * EncryptedPrivateKey encryptedPrivateKey.
         * @member {Uint8Array} encryptedPrivateKey
         * @memberof keys.EncryptedPrivateKey
         * @instance
         */
        EncryptedPrivateKey.prototype.encryptedPrivateKey = $util.newBuffer([]);

        /**
         * EncryptedPrivateKey uid.
         * @member {string} uid
         * @memberof keys.EncryptedPrivateKey
         * @instance
         */
        EncryptedPrivateKey.prototype.uid = "";

        /**
         * EncryptedPrivateKey email.
         * @member {string} email
         * @memberof keys.EncryptedPrivateKey
         * @instance
         */
        EncryptedPrivateKey.prototype.email = "";

        /**
         * EncryptedPrivateKey issuedAt.
         * @member {number|Long} issuedAt
         * @memberof keys.EncryptedPrivateKey
         * @instance
         */
        EncryptedPrivateKey.prototype.issuedAt = $util.Long ? $util.Long.fromBits(0,0,false) : 0;

        /**
         * Creates a new EncryptedPrivateKey instance using the specified properties.
         * @function create
         * @memberof keys.EncryptedPrivateKey
         * @static
         * @param {keys.IEncryptedPrivateKey=} [properties] Properties to set
         * @returns {keys.EncryptedPrivateKey} EncryptedPrivateKey instance
         */
        EncryptedPrivateKey.create = function create(properties) {
            return new EncryptedPrivateKey(properties);
        };

        /**
         * Encodes the specified EncryptedPrivateKey message. Does not implicitly {@link keys.EncryptedPrivateKey.verify|verify} messages.
         * @function encode
         * @memberof keys.EncryptedPrivateKey
         * @static
         * @param {keys.IEncryptedPrivateKey} message EncryptedPrivateKey message or plain object to encode
         * @param {$protobuf.Writer} [writer] Writer to encode to
         * @returns {$protobuf.Writer} Writer
         */
        EncryptedPrivateKey.encode = function encode(message, writer) {
            if (!writer)
                writer = $Writer.create();
            if (message.version != null && Object.hasOwnProperty.call(message, "version"))
                writer.uint32(/* id 1, wireType 0 =*/8).int32(message.version);
            if (message.encryptedPrivateKey != null && Object.hasOwnProperty.call(message, "encryptedPrivateKey"))
                writer.uint32(/* id 2, wireType 2 =*/18).bytes(message.encryptedPrivateKey);
            if (message.uid != null && Object.hasOwnProperty.call(message, "uid"))
                writer.uint32(/* id 3, wireType 2 =*/26).string(message.uid);
            if (message.email != null && Object.hasOwnProperty.call(message, "email"))
                writer.uint32(/* id 4, wireType 2 =*/34).string(message.email);
            if (message.issuedAt != null && Object.hasOwnProperty.call(message, "issuedAt"))
                writer.uint32(/* id 5, wireType 0 =*/40).int64(message.issuedAt);
            return writer;
        };

        /**
         * Encodes the specified EncryptedPrivateKey message, length delimited. Does not implicitly {@link keys.EncryptedPrivateKey.verify|verify} messages.
         * @function encodeDelimited
         * @memberof keys.EncryptedPrivateKey
         * @static
         * @param {keys.IEncryptedPrivateKey} message EncryptedPrivateKey message or plain object to encode
         * @param {$protobuf.Writer} [writer] Writer to encode to
         * @returns {$protobuf.Writer} Writer
         */
        EncryptedPrivateKey.encodeDelimited = function encodeDelimited(message, writer) {
            return this.encode(message, writer).ldelim();
        };

        /**
         * Decodes an EncryptedPrivateKey message from the specified reader or buffer.
         * @function decode
         * @memberof keys.EncryptedPrivateKey
         * @static
         * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
         * @param {number} [length] Message length if known beforehand
         * @returns {keys.EncryptedPrivateKey} EncryptedPrivateKey
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        EncryptedPrivateKey.decode = function decode(reader, length) {
            if (!(reader instanceof $Reader))
                reader = $Reader.create(reader);
            var end = length === undefined ? reader.len : reader.pos + length, message = new $root.keys.EncryptedPrivateKey();
            while (reader.pos < end) {
                var tag = reader.uint32();
                switch (tag >>> 3) {
                case 1: {
                        message.version = reader.int32();
                        break;
                    }
                case 2: {
                        message.encryptedPrivateKey = reader.bytes();
                        break;
                    }
                case 3: {
                        message.uid = reader.string();
                        break;
                    }
                case 4: {
                        message.email = reader.string();
                        break;
                    }
                case 5: {
                        message.issuedAt = reader.int64();
                        break;
                    }
                default:
                    reader.skipType(tag & 7);
                    break;
                }
            }
            return message;
        };

        /**
         * Decodes an EncryptedPrivateKey message from the specified reader or buffer, length delimited.
         * @function decodeDelimited
         * @memberof keys.EncryptedPrivateKey
         * @static
         * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
         * @returns {keys.EncryptedPrivateKey} EncryptedPrivateKey
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        EncryptedPrivateKey.decodeDelimited = function decodeDelimited(reader) {
            if (!(reader instanceof $Reader))
                reader = new $Reader(reader);
            return this.decode(reader, reader.uint32());
        };

        /**
         * Verifies an EncryptedPrivateKey message.
         * @function verify
         * @memberof keys.EncryptedPrivateKey
         * @static
         * @param {Object.<string,*>} message Plain object to verify
         * @returns {string|null} `null` if valid, otherwise the reason why it is not
         */
        EncryptedPrivateKey.verify = function verify(message) {
            if (typeof message !== "object" || message === null)
                return "object expected";
            if (message.version != null && message.hasOwnProperty("version"))
                if (!$util.isInteger(message.version))
                    return "version: integer expected";
            if (message.encryptedPrivateKey != null && message.hasOwnProperty("encryptedPrivateKey"))
                if (!(message.encryptedPrivateKey && typeof message.encryptedPrivateKey.length === "number" || $util.isString(message.encryptedPrivateKey)))
                    return "encryptedPrivateKey: buffer expected";
            if (message.uid != null && message.hasOwnProperty("uid"))
                if (!$util.isString(message.uid))
                    return "uid: string expected";
            if (message.email != null && message.hasOwnProperty("email"))
                if (!$util.isString(message.email))
                    return "email: string expected";
            if (message.issuedAt != null && message.hasOwnProperty("issuedAt"))
                if (!$util.isInteger(message.issuedAt) && !(message.issuedAt && $util.isInteger(message.issuedAt.low) && $util.isInteger(message.issuedAt.high)))
                    return "issuedAt: integer|Long expected";
            return null;
        };

        /**
         * Creates an EncryptedPrivateKey message from a plain object. Also converts values to their respective internal types.
         * @function fromObject
         * @memberof keys.EncryptedPrivateKey
         * @static
         * @param {Object.<string,*>} object Plain object
         * @returns {keys.EncryptedPrivateKey} EncryptedPrivateKey
         */
        EncryptedPrivateKey.fromObject = function fromObject(object) {
            if (object instanceof $root.keys.EncryptedPrivateKey)
                return object;
            var message = new $root.keys.EncryptedPrivateKey();
            if (object.version != null)
                message.version = object.version | 0;
            if (object.encryptedPrivateKey != null)
                if (typeof object.encryptedPrivateKey === "string")
                    $util.base64.decode(object.encryptedPrivateKey, message.encryptedPrivateKey = $util.newBuffer($util.base64.length(object.encryptedPrivateKey)), 0);
                else if (object.encryptedPrivateKey.length >= 0)
                    message.encryptedPrivateKey = object.encryptedPrivateKey;
            if (object.uid != null)
                message.uid = String(object.uid);
            if (object.email != null)
                message.email = String(object.email);
            if (object.issuedAt != null)
                if ($util.Long)
                    (message.issuedAt = $util.Long.fromValue(object.issuedAt)).unsigned = false;
                else if (typeof object.issuedAt === "string")
                    message.issuedAt = parseInt(object.issuedAt, 10);
                else if (typeof object.issuedAt === "number")
                    message.issuedAt = object.issuedAt;
                else if (typeof object.issuedAt === "object")
                    message.issuedAt = new $util.LongBits(object.issuedAt.low >>> 0, object.issuedAt.high >>> 0).toNumber();
            return message;
        };

        /**
         * Creates a plain object from an EncryptedPrivateKey message. Also converts values to other types if specified.
         * @function toObject
         * @memberof keys.EncryptedPrivateKey
         * @static
         * @param {keys.EncryptedPrivateKey} message EncryptedPrivateKey
         * @param {$protobuf.IConversionOptions} [options] Conversion options
         * @returns {Object.<string,*>} Plain object
         */
        EncryptedPrivateKey.toObject = function toObject(message, options) {
            if (!options)
                options = {};
            var object = {};
            if (options.defaults) {
                object.version = 0;
                if (options.bytes === String)
                    object.encryptedPrivateKey = "";
                else {
                    object.encryptedPrivateKey = [];
                    if (options.bytes !== Array)
                        object.encryptedPrivateKey = $util.newBuffer(object.encryptedPrivateKey);
                }
                object.uid = "";
                object.email = "";
                if ($util.Long) {
                    var long = new $util.Long(0, 0, false);
                    object.issuedAt = options.longs === String ? long.toString() : options.longs === Number ? long.toNumber() : long;
                } else
                    object.issuedAt = options.longs === String ? "0" : 0;
            }
            if (message.version != null && message.hasOwnProperty("version"))
                object.version = message.version;
            if (message.encryptedPrivateKey != null && message.hasOwnProperty("encryptedPrivateKey"))
                object.encryptedPrivateKey = options.bytes === String ? $util.base64.encode(message.encryptedPrivateKey, 0, message.encryptedPrivateKey.length) : options.bytes === Array ? Array.prototype.slice.call(message.encryptedPrivateKey) : message.encryptedPrivateKey;
            if (message.uid != null && message.hasOwnProperty("uid"))
                object.uid = message.uid;
            if (message.email != null && message.hasOwnProperty("email"))
                object.email = message.email;
            if (message.issuedAt != null && message.hasOwnProperty("issuedAt"))
                if (typeof message.issuedAt === "number")
                    object.issuedAt = options.longs === String ? String(message.issuedAt) : message.issuedAt;
                else
                    object.issuedAt = options.longs === String ? $util.Long.prototype.toString.call(message.issuedAt) : options.longs === Number ? new $util.LongBits(message.issuedAt.low >>> 0, message.issuedAt.high >>> 0).toNumber() : message.issuedAt;
            return object;
        };

        /**
         * Converts this EncryptedPrivateKey to JSON.
         * @function toJSON
         * @memberof keys.EncryptedPrivateKey
         * @instance
         * @returns {Object.<string,*>} JSON object
         */
        EncryptedPrivateKey.prototype.toJSON = function toJSON() {
            return this.constructor.toObject(this, $protobuf.util.toJSONOptions);
        };

        /**
         * Gets the default type url for EncryptedPrivateKey
         * @function getTypeUrl
         * @memberof keys.EncryptedPrivateKey
         * @static
         * @param {string} [typeUrlPrefix] your custom typeUrlPrefix(default "type.googleapis.com")
         * @returns {string} The default type url
         */
        EncryptedPrivateKey.getTypeUrl = function getTypeUrl(typeUrlPrefix) {
            if (typeUrlPrefix === undefined) {
                typeUrlPrefix = "type.googleapis.com";
            }
            return typeUrlPrefix + "/keys.EncryptedPrivateKey";
        };

        return EncryptedPrivateKey;
    })();

    keys.SignedEncryptedPrivateKey = (function() {

        /**
         * Properties of a SignedEncryptedPrivateKey.
         * @memberof keys
         * @interface ISignedEncryptedPrivateKey
         * @property {number|null} [version] SignedEncryptedPrivateKey version
         * @property {Uint8Array|null} [encryptedPrivateKey] SignedEncryptedPrivateKey encryptedPrivateKey
         * @property {Uint8Array|null} [signature] SignedEncryptedPrivateKey signature
         */

        /**
         * Constructs a new SignedEncryptedPrivateKey.
         * @memberof keys
         * @classdesc Represents a SignedEncryptedPrivateKey.
         * @implements ISignedEncryptedPrivateKey
         * @constructor
         * @param {keys.ISignedEncryptedPrivateKey=} [properties] Properties to set
         */
        function SignedEncryptedPrivateKey(properties) {
            if (properties)
                for (var keys = Object.keys(properties), i = 0; i < keys.length; ++i)
                    if (properties[keys[i]] != null)
                        this[keys[i]] = properties[keys[i]];
        }

        /**
         * SignedEncryptedPrivateKey version.
         * @member {number} version
         * @memberof keys.SignedEncryptedPrivateKey
         * @instance
         */
        SignedEncryptedPrivateKey.prototype.version = 0;

        /**
         * SignedEncryptedPrivateKey encryptedPrivateKey.
         * @member {Uint8Array} encryptedPrivateKey
         * @memberof keys.SignedEncryptedPrivateKey
         * @instance
         */
        SignedEncryptedPrivateKey.prototype.encryptedPrivateKey = $util.newBuffer([]);

        /**
         * SignedEncryptedPrivateKey signature.
         * @member {Uint8Array} signature
         * @memberof keys.SignedEncryptedPrivateKey
         * @instance
         */
        SignedEncryptedPrivateKey.prototype.signature = $util.newBuffer([]);

        /**
         * Creates a new SignedEncryptedPrivateKey instance using the specified properties.
         * @function create
         * @memberof keys.SignedEncryptedPrivateKey
         * @static
         * @param {keys.ISignedEncryptedPrivateKey=} [properties] Properties to set
         * @returns {keys.SignedEncryptedPrivateKey} SignedEncryptedPrivateKey instance
         */
        SignedEncryptedPrivateKey.create = function create(properties) {
            return new SignedEncryptedPrivateKey(properties);
        };

        /**
         * Encodes the specified SignedEncryptedPrivateKey message. Does not implicitly {@link keys.SignedEncryptedPrivateKey.verify|verify} messages.
         * @function encode
         * @memberof keys.SignedEncryptedPrivateKey
         * @static
         * @param {keys.ISignedEncryptedPrivateKey} message SignedEncryptedPrivateKey message or plain object to encode
         * @param {$protobuf.Writer} [writer] Writer to encode to
         * @returns {$protobuf.Writer} Writer
         */
        SignedEncryptedPrivateKey.encode = function encode(message, writer) {
            if (!writer)
                writer = $Writer.create();
            if (message.version != null && Object.hasOwnProperty.call(message, "version"))
                writer.uint32(/* id 1, wireType 0 =*/8).int32(message.version);
            if (message.encryptedPrivateKey != null && Object.hasOwnProperty.call(message, "encryptedPrivateKey"))
                writer.uint32(/* id 2, wireType 2 =*/18).bytes(message.encryptedPrivateKey);
            if (message.signature != null && Object.hasOwnProperty.call(message, "signature"))
                writer.uint32(/* id 3, wireType 2 =*/26).bytes(message.signature);
            return writer;
        };

        /**
         * Encodes the specified SignedEncryptedPrivateKey message, length delimited. Does not implicitly {@link keys.SignedEncryptedPrivateKey.verify|verify} messages.
         * @function encodeDelimited
         * @memberof keys.SignedEncryptedPrivateKey
         * @static
         * @param {keys.ISignedEncryptedPrivateKey} message SignedEncryptedPrivateKey message or plain object to encode
         * @param {$protobuf.Writer} [writer] Writer to encode to
         * @returns {$protobuf.Writer} Writer
         */
        SignedEncryptedPrivateKey.encodeDelimited = function encodeDelimited(message, writer) {
            return this.encode(message, writer).ldelim();
        };

        /**
         * Decodes a SignedEncryptedPrivateKey message from the specified reader or buffer.
         * @function decode
         * @memberof keys.SignedEncryptedPrivateKey
         * @static
         * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
         * @param {number} [length] Message length if known beforehand
         * @returns {keys.SignedEncryptedPrivateKey} SignedEncryptedPrivateKey
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        SignedEncryptedPrivateKey.decode = function decode(reader, length) {
            if (!(reader instanceof $Reader))
                reader = $Reader.create(reader);
            var end = length === undefined ? reader.len : reader.pos + length, message = new $root.keys.SignedEncryptedPrivateKey();
            while (reader.pos < end) {
                var tag = reader.uint32();
                switch (tag >>> 3) {
                case 1: {
                        message.version = reader.int32();
                        break;
                    }
                case 2: {
                        message.encryptedPrivateKey = reader.bytes();
                        break;
                    }
                case 3: {
                        message.signature = reader.bytes();
                        break;
                    }
                default:
                    reader.skipType(tag & 7);
                    break;
                }
            }
            return message;
        };

        /**
         * Decodes a SignedEncryptedPrivateKey message from the specified reader or buffer, length delimited.
         * @function decodeDelimited
         * @memberof keys.SignedEncryptedPrivateKey
         * @static
         * @param {$protobuf.Reader|Uint8Array} reader Reader or buffer to decode from
         * @returns {keys.SignedEncryptedPrivateKey} SignedEncryptedPrivateKey
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        SignedEncryptedPrivateKey.decodeDelimited = function decodeDelimited(reader) {
            if (!(reader instanceof $Reader))
                reader = new $Reader(reader);
            return this.decode(reader, reader.uint32());
        };

        /**
         * Verifies a SignedEncryptedPrivateKey message.
         * @function verify
         * @memberof keys.SignedEncryptedPrivateKey
         * @static
         * @param {Object.<string,*>} message Plain object to verify
         * @returns {string|null} `null` if valid, otherwise the reason why it is not
         */
        SignedEncryptedPrivateKey.verify = function verify(message) {
            if (typeof message !== "object" || message === null)
                return "object expected";
            if (message.version != null && message.hasOwnProperty("version"))
                if (!$util.isInteger(message.version))
                    return "version: integer expected";
            if (message.encryptedPrivateKey != null && message.hasOwnProperty("encryptedPrivateKey"))
                if (!(message.encryptedPrivateKey && typeof message.encryptedPrivateKey.length === "number" || $util.isString(message.encryptedPrivateKey)))
                    return "encryptedPrivateKey: buffer expected";
            if (message.signature != null && message.hasOwnProperty("signature"))
                if (!(message.signature && typeof message.signature.length === "number" || $util.isString(message.signature)))
                    return "signature: buffer expected";
            return null;
        };

        /**
         * Creates a SignedEncryptedPrivateKey message from a plain object. Also converts values to their respective internal types.
         * @function fromObject
         * @memberof keys.SignedEncryptedPrivateKey
         * @static
         * @param {Object.<string,*>} object Plain object
         * @returns {keys.SignedEncryptedPrivateKey} SignedEncryptedPrivateKey
         */
        SignedEncryptedPrivateKey.fromObject = function fromObject(object) {
            if (object instanceof $root.keys.SignedEncryptedPrivateKey)
                return object;
            var message = new $root.keys.SignedEncryptedPrivateKey();
            if (object.version != null)
                message.version = object.version | 0;
            if (object.encryptedPrivateKey != null)
                if (typeof object.encryptedPrivateKey === "string")
                    $util.base64.decode(object.encryptedPrivateKey, message.encryptedPrivateKey = $util.newBuffer($util.base64.length(object.encryptedPrivateKey)), 0);
                else if (object.encryptedPrivateKey.length >= 0)
                    message.encryptedPrivateKey = object.encryptedPrivateKey;
            if (object.signature != null)
                if (typeof object.signature === "string")
                    $util.base64.decode(object.signature, message.signature = $util.newBuffer($util.base64.length(object.signature)), 0);
                else if (object.signature.length >= 0)
                    message.signature = object.signature;
            return message;
        };

        /**
         * Creates a plain object from a SignedEncryptedPrivateKey message. Also converts values to other types if specified.
         * @function toObject
         * @memberof keys.SignedEncryptedPrivateKey
         * @static
         * @param {keys.SignedEncryptedPrivateKey} message SignedEncryptedPrivateKey
         * @param {$protobuf.IConversionOptions} [options] Conversion options
         * @returns {Object.<string,*>} Plain object
         */
        SignedEncryptedPrivateKey.toObject = function toObject(message, options) {
            if (!options)
                options = {};
            var object = {};
            if (options.defaults) {
                object.version = 0;
                if (options.bytes === String)
                    object.encryptedPrivateKey = "";
                else {
                    object.encryptedPrivateKey = [];
                    if (options.bytes !== Array)
                        object.encryptedPrivateKey = $util.newBuffer(object.encryptedPrivateKey);
                }
                if (options.bytes === String)
                    object.signature = "";
                else {
                    object.signature = [];
                    if (options.bytes !== Array)
                        object.signature = $util.newBuffer(object.signature);
                }
            }
            if (message.version != null && message.hasOwnProperty("version"))
                object.version = message.version;
            if (message.encryptedPrivateKey != null && message.hasOwnProperty("encryptedPrivateKey"))
                object.encryptedPrivateKey = options.bytes === String ? $util.base64.encode(message.encryptedPrivateKey, 0, message.encryptedPrivateKey.length) : options.bytes === Array ? Array.prototype.slice.call(message.encryptedPrivateKey) : message.encryptedPrivateKey;
            if (message.signature != null && message.hasOwnProperty("signature"))
                object.signature = options.bytes === String ? $util.base64.encode(message.signature, 0, message.signature.length) : options.bytes === Array ? Array.prototype.slice.call(message.signature) : message.signature;
            return object;
        };

        /**
         * Converts this SignedEncryptedPrivateKey to JSON.
         * @function toJSON
         * @memberof keys.SignedEncryptedPrivateKey
         * @instance
         * @returns {Object.<string,*>} JSON object
         */
        SignedEncryptedPrivateKey.prototype.toJSON = function toJSON() {
            return this.constructor.toObject(this, $protobuf.util.toJSONOptions);
        };

        /**
         * Gets the default type url for SignedEncryptedPrivateKey
         * @function getTypeUrl
         * @memberof keys.SignedEncryptedPrivateKey
         * @static
         * @param {string} [typeUrlPrefix] your custom typeUrlPrefix(default "type.googleapis.com")
         * @returns {string} The default type url
         */
        SignedEncryptedPrivateKey.getTypeUrl = function getTypeUrl(typeUrlPrefix) {
            if (typeUrlPrefix === undefined) {
                typeUrlPrefix = "type.googleapis.com";
            }
            return typeUrlPrefix + "/keys.SignedEncryptedPrivateKey";
        };

        return SignedEncryptedPrivateKey;
    })();

    return keys;
})();

module.exports = $root;
