import * as $protobuf from "protobufjs";
import Long = require("long");
/** Namespace keys. */
export namespace keys {

    /** Properties of an EncryptedPrivateKey. */
    interface IEncryptedPrivateKey {

        /** EncryptedPrivateKey version */
        version?: (number|null);

        /** EncryptedPrivateKey encryptedPrivateKey */
        encryptedPrivateKey?: (Uint8Array|null);

        /** EncryptedPrivateKey uid */
        uid?: (string|null);

        /** EncryptedPrivateKey email */
        email?: (string|null);

        /** EncryptedPrivateKey issuedAt */
        issuedAt?: (number|Long|null);
    }

    /** Represents an EncryptedPrivateKey. */
    class EncryptedPrivateKey implements IEncryptedPrivateKey {

        /**
         * Constructs a new EncryptedPrivateKey.
         * @param [properties] Properties to set
         */
        constructor(properties?: keys.IEncryptedPrivateKey);

        /** EncryptedPrivateKey version. */
        public version: number;

        /** EncryptedPrivateKey encryptedPrivateKey. */
        public encryptedPrivateKey: Uint8Array;

        /** EncryptedPrivateKey uid. */
        public uid: string;

        /** EncryptedPrivateKey email. */
        public email: string;

        /** EncryptedPrivateKey issuedAt. */
        public issuedAt: (number|Long);

        /**
         * Creates a new EncryptedPrivateKey instance using the specified properties.
         * @param [properties] Properties to set
         * @returns EncryptedPrivateKey instance
         */
        public static create(properties?: keys.IEncryptedPrivateKey): keys.EncryptedPrivateKey;

        /**
         * Encodes the specified EncryptedPrivateKey message. Does not implicitly {@link keys.EncryptedPrivateKey.verify|verify} messages.
         * @param message EncryptedPrivateKey message or plain object to encode
         * @param [writer] Writer to encode to
         * @returns Writer
         */
        public static encode(message: keys.IEncryptedPrivateKey, writer?: $protobuf.Writer): $protobuf.Writer;

        /**
         * Encodes the specified EncryptedPrivateKey message, length delimited. Does not implicitly {@link keys.EncryptedPrivateKey.verify|verify} messages.
         * @param message EncryptedPrivateKey message or plain object to encode
         * @param [writer] Writer to encode to
         * @returns Writer
         */
        public static encodeDelimited(message: keys.IEncryptedPrivateKey, writer?: $protobuf.Writer): $protobuf.Writer;

        /**
         * Decodes an EncryptedPrivateKey message from the specified reader or buffer.
         * @param reader Reader or buffer to decode from
         * @param [length] Message length if known beforehand
         * @returns EncryptedPrivateKey
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        public static decode(reader: ($protobuf.Reader|Uint8Array), length?: number): keys.EncryptedPrivateKey;

        /**
         * Decodes an EncryptedPrivateKey message from the specified reader or buffer, length delimited.
         * @param reader Reader or buffer to decode from
         * @returns EncryptedPrivateKey
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        public static decodeDelimited(reader: ($protobuf.Reader|Uint8Array)): keys.EncryptedPrivateKey;

        /**
         * Verifies an EncryptedPrivateKey message.
         * @param message Plain object to verify
         * @returns `null` if valid, otherwise the reason why it is not
         */
        public static verify(message: { [k: string]: any }): (string|null);

        /**
         * Creates an EncryptedPrivateKey message from a plain object. Also converts values to their respective internal types.
         * @param object Plain object
         * @returns EncryptedPrivateKey
         */
        public static fromObject(object: { [k: string]: any }): keys.EncryptedPrivateKey;

        /**
         * Creates a plain object from an EncryptedPrivateKey message. Also converts values to other types if specified.
         * @param message EncryptedPrivateKey
         * @param [options] Conversion options
         * @returns Plain object
         */
        public static toObject(message: keys.EncryptedPrivateKey, options?: $protobuf.IConversionOptions): { [k: string]: any };

        /**
         * Converts this EncryptedPrivateKey to JSON.
         * @returns JSON object
         */
        public toJSON(): { [k: string]: any };

        /**
         * Gets the default type url for EncryptedPrivateKey
         * @param [typeUrlPrefix] your custom typeUrlPrefix(default "type.googleapis.com")
         * @returns The default type url
         */
        public static getTypeUrl(typeUrlPrefix?: string): string;
    }

    /** Properties of a SignedEncryptedPrivateKey. */
    interface ISignedEncryptedPrivateKey {

        /** SignedEncryptedPrivateKey version */
        version?: (number|null);

        /** SignedEncryptedPrivateKey encryptedPrivateKey */
        encryptedPrivateKey?: (Uint8Array|null);

        /** SignedEncryptedPrivateKey signature */
        signature?: (Uint8Array|null);
    }

    /** Represents a SignedEncryptedPrivateKey. */
    class SignedEncryptedPrivateKey implements ISignedEncryptedPrivateKey {

        /**
         * Constructs a new SignedEncryptedPrivateKey.
         * @param [properties] Properties to set
         */
        constructor(properties?: keys.ISignedEncryptedPrivateKey);

        /** SignedEncryptedPrivateKey version. */
        public version: number;

        /** SignedEncryptedPrivateKey encryptedPrivateKey. */
        public encryptedPrivateKey: Uint8Array;

        /** SignedEncryptedPrivateKey signature. */
        public signature: Uint8Array;

        /**
         * Creates a new SignedEncryptedPrivateKey instance using the specified properties.
         * @param [properties] Properties to set
         * @returns SignedEncryptedPrivateKey instance
         */
        public static create(properties?: keys.ISignedEncryptedPrivateKey): keys.SignedEncryptedPrivateKey;

        /**
         * Encodes the specified SignedEncryptedPrivateKey message. Does not implicitly {@link keys.SignedEncryptedPrivateKey.verify|verify} messages.
         * @param message SignedEncryptedPrivateKey message or plain object to encode
         * @param [writer] Writer to encode to
         * @returns Writer
         */
        public static encode(message: keys.ISignedEncryptedPrivateKey, writer?: $protobuf.Writer): $protobuf.Writer;

        /**
         * Encodes the specified SignedEncryptedPrivateKey message, length delimited. Does not implicitly {@link keys.SignedEncryptedPrivateKey.verify|verify} messages.
         * @param message SignedEncryptedPrivateKey message or plain object to encode
         * @param [writer] Writer to encode to
         * @returns Writer
         */
        public static encodeDelimited(message: keys.ISignedEncryptedPrivateKey, writer?: $protobuf.Writer): $protobuf.Writer;

        /**
         * Decodes a SignedEncryptedPrivateKey message from the specified reader or buffer.
         * @param reader Reader or buffer to decode from
         * @param [length] Message length if known beforehand
         * @returns SignedEncryptedPrivateKey
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        public static decode(reader: ($protobuf.Reader|Uint8Array), length?: number): keys.SignedEncryptedPrivateKey;

        /**
         * Decodes a SignedEncryptedPrivateKey message from the specified reader or buffer, length delimited.
         * @param reader Reader or buffer to decode from
         * @returns SignedEncryptedPrivateKey
         * @throws {Error} If the payload is not a reader or valid buffer
         * @throws {$protobuf.util.ProtocolError} If required fields are missing
         */
        public static decodeDelimited(reader: ($protobuf.Reader|Uint8Array)): keys.SignedEncryptedPrivateKey;

        /**
         * Verifies a SignedEncryptedPrivateKey message.
         * @param message Plain object to verify
         * @returns `null` if valid, otherwise the reason why it is not
         */
        public static verify(message: { [k: string]: any }): (string|null);

        /**
         * Creates a SignedEncryptedPrivateKey message from a plain object. Also converts values to their respective internal types.
         * @param object Plain object
         * @returns SignedEncryptedPrivateKey
         */
        public static fromObject(object: { [k: string]: any }): keys.SignedEncryptedPrivateKey;

        /**
         * Creates a plain object from a SignedEncryptedPrivateKey message. Also converts values to other types if specified.
         * @param message SignedEncryptedPrivateKey
         * @param [options] Conversion options
         * @returns Plain object
         */
        public static toObject(message: keys.SignedEncryptedPrivateKey, options?: $protobuf.IConversionOptions): { [k: string]: any };

        /**
         * Converts this SignedEncryptedPrivateKey to JSON.
         * @returns JSON object
         */
        public toJSON(): { [k: string]: any };

        /**
         * Gets the default type url for SignedEncryptedPrivateKey
         * @param [typeUrlPrefix] your custom typeUrlPrefix(default "type.googleapis.com")
         * @returns The default type url
         */
        public static getTypeUrl(typeUrlPrefix?: string): string;
    }
}
