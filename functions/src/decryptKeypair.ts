import { HttpsError } from "firebase-functions/v1/auth"
import * as z from "zod"

import { VerifiedAuthData } from "./middleware/auth"
import { decryptPrivateKey } from "./service/keys"

export const DecryptKeypairRequest = z.object({
  encrypted_private_key: z.string(),
})
export type DecryptKeypairRequest = z.infer<typeof DecryptKeypairRequest>

/**
 * Decrypt a user's private key using KMS
 * @param data Request with encrypted private key
 * @param auth USer info
 */
export const decryptKeypair = async (
  data: DecryptKeypairRequest,
  auth: VerifiedAuthData
) => {
  const request = DecryptKeypairRequest.safeParse(data)

  if (!request.success) {
    throw new HttpsError(
      "invalid-argument",
      "Encryption request is malformed",
      request.error
    )
  }

  const { data: keyData } = request

  let plainPrivateKey: Buffer

  try {
    plainPrivateKey = await decryptPrivateKey(
      auth.uid,
      auth.token.email,
      keyData.encrypted_private_key
    )
  } catch (e) {
    console.error("Error during private key decryption", e)
    throw new HttpsError(
      "invalid-argument",
      "Cannot decrypt the supplied ciphertext"
    )
  }

  return {
    private_key: plainPrivateKey.toString("base64url"),
  }
}
