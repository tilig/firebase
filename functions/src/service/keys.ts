import { crc32c } from "@node-rs/crc32"

import {
  cryptoClient,
  keyEncryptionKeyName,
  signingKeyName,
} from "../config/environment"

import {
  decodeEncryptedPrivateKey,
  decodeSignedPrivateKey,
  encodeEncryptedPrivateKey,
  encodeSignedPrivateKey,
} from "./encoding"
import { decrypt, encrypt, sign, verify } from "./crypto"

class ClientError implements Error {
  name = "ClientError"
  constructor(public message: string) {}
}

export interface RemotePublicKey {
  name: string
  pem: string
}

const getPublicKey = async (keyName: string) => {
  const [publicKey] = await cryptoClient.getPublicKey({
    name: keyName,
  })

  if (publicKey.name !== keyName) {
    throw new ClientError(`Error obtaining key ${publicKey.name} != ${keyName}`)
  }

  if (
    !publicKey.pem ||
    !publicKey.pemCrc32c?.value ||
    crc32c(publicKey.pem) !== Number(publicKey.pemCrc32c.value)
  ) {
    console.error(
      "Public key response corrupted in-transit",
      publicKey.pem ? crc32c(publicKey.pem) : "no pem",
      publicKey.pemCrc32c?.value
    )
    throw new Error("Public key response corrupted in-transit")
  }

  return publicKey as RemotePublicKey
}

let currentKekPublicKey: RemotePublicKey
let currentSigningPublicKey: RemotePublicKey

const getCurrentKekPublicKey = async () => {
  if (currentKekPublicKey) return currentKekPublicKey

  currentKekPublicKey = await getPublicKey(keyEncryptionKeyName.value())
  return currentKekPublicKey
}

const getCurrentSigningPublicKey = async () => {
  if (currentSigningPublicKey) return currentSigningPublicKey

  currentSigningPublicKey = await getPublicKey(signingKeyName.value())
  return currentSigningPublicKey
}

export const clearBuf = (buf: Buffer) => buf.fill(0)

export const encryptPrivateKey = async (
  uid: string,
  email: string,
  plainPrivateKeyString: string
) => {
  const privateKey = Buffer.from(plainPrivateKeyString, "base64url")

  // Encrypt the private key, so it's safe for storage
  const kekPublicKey = await getCurrentKekPublicKey()
  const ciphertext = await encrypt(privateKey, kekPublicKey.pem)

  // Clear sensitive data within this function
  plainPrivateKeyString = ""
  // Clear private key buffer
  clearBuf(privateKey)

  // Wrap the encrypted private key with meta data in a Protobuf message
  const encryptedPrivateKey = encodeEncryptedPrivateKey({
    encryptedPrivateKey: ciphertext,
    uid,
    email,
  })

  // Sign the encrypted key and meta data
  const signature = await sign(encryptedPrivateKey, signingKeyName.value())

  // Wrap the message and signature in a Protobuf message
  const signedPrivateKey = encodeSignedPrivateKey({
    encryptedPrivateKey,
    signature,
  })

  return signedPrivateKey
}

class PermissionError implements Error {
  name = "PermissionError"
  constructor(public message: string) {}
}

export const decryptPrivateKey = async (
  uid: string,
  email: string,
  encryptedPrivateKeyString: string
) => {
  // Extra check to make sure we have a uid and an email present
  if (!uid || !email) {
    throw new PermissionError(
      "User does not have permission to decrypt this key"
    )
  }

  const encoded = Buffer.from(encryptedPrivateKeyString, "base64url")
  // Parse protobuf message
  const signedPrivateKey = decodeSignedPrivateKey(encoded)

  const signingPublicKey = await getCurrentSigningPublicKey()

  if (
    !verify(
      signedPrivateKey.encryptedPrivateKey as Buffer,
      signedPrivateKey.signature as Buffer,
      signingPublicKey.pem
    )
  ) {
    // This code will never be reached, since verify will throw, but as an extra precausion we throw here as well
    throw new PermissionError(
      "User does not have permission to decrypt this key"
    )
  }

  // Parse protobuf message
  const encryptedPrivateKey = decodeEncryptedPrivateKey(
    signedPrivateKey.encryptedPrivateKey as Buffer
  )

  /* ==================================================== */
  /* THIS IS THE MOST IMPORTANT CHECK OF THIS ENTIRE      */
  /* PROGRAM. THIS IS WHAT DETERMINES IF WE WILL DECRYPT  */
  /* THE PRIVATE KEY FOR THE USER.                        */
  /*                                                      */
  /* THIS IS OF THE UTTERMOST IMPORTANCE AND IT WILL      */
  /* LEAK PRIVATE KEYS UNLESS IT IS IMPLEMENTED CORRECTLY */
  /*                                                      */
  /* TAKE EXTREME CARE WHEN MODIFYING ANYTHING HERE       */
  /* ==================================================== */
  if (encryptedPrivateKey.uid !== uid) {
    throw new PermissionError(
      "User does not have the UID required to decrypt this key"
    )
  }

  if (encryptedPrivateKey.email !== email) {
    throw new PermissionError(
      "User does not have the email required to decrypt this key"
    )
  }
  /* ==================================================== */
  /* END OF ACCESS CHECKS                                 */
  /* ==================================================== */

  // Decrypt private key
  const plainPrivateKey = await decrypt(
    encryptedPrivateKey.encryptedPrivateKey as Buffer,
    keyEncryptionKeyName.value()
  )

  return plainPrivateKey
}
