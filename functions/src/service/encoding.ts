import { keys } from "../protocol"

const { EncryptedPrivateKey, SignedEncryptedPrivateKey } = keys
type EncryptedPrivateKey = typeof EncryptedPrivateKey

const epoch = (date: Date) => Math.floor(date.getTime() / 1000)

interface EncryptedPrivateKeyWithMeta {
  encryptedPrivateKey: Buffer
  uid: string
  email: string
  issuedAt?: number
}

interface SignedPrivateKey {
  encryptedPrivateKey: Buffer
  signature: Buffer
}

class EncodingError implements Error {
  name = "EncodingError"
  constructor(public message: string) {}
}

/**
 * Encode an encrypted private key with meta data into a Protobuf message
 * @param data To be encoded properties
 * @returns Buffer from protobuf message
 */
export const encodeEncryptedPrivateKey = (
  data: EncryptedPrivateKeyWithMeta
) => {
  let { issuedAt, ...payload } = data
  if (!issuedAt) {
    issuedAt = epoch(new Date())
  }
  const msg = EncryptedPrivateKey.create({
    ...payload,
    issuedAt,
  })

  const err = EncryptedPrivateKey.verify(msg)
  if (err !== null) {
    throw new EncodingError(`Could not encode private key: ${err}`)
  }

  return EncryptedPrivateKey.encode(msg).finish() as Buffer
}

/**
 * Parse encrypted private key with meta data message
 * @param encoded Buffer with protobuf message
 */
export const decodeEncryptedPrivateKey = (encoded: Buffer) => {
  return EncryptedPrivateKey.decode(encoded)
}

/**
 * Encode encrypted private key and signature into protobuf message
 * @param data Properties to be encoded
 */
export const encodeSignedPrivateKey = (data: SignedPrivateKey) => {
  const msg = SignedEncryptedPrivateKey.create(data)

  const err = SignedEncryptedPrivateKey.verify(msg)
  if (err !== null) {
    throw new EncodingError(`Could not encode private key: ${err}`)
  }

  return SignedEncryptedPrivateKey.encode(msg).finish() as Buffer
}

/**
 * Decode signature and encrypted private key from protobuf message
 * @param encoded Protobuf message
 */
export const decodeSignedPrivateKey = (encoded: Buffer) => {
  return SignedEncryptedPrivateKey.decode(encoded)
}
