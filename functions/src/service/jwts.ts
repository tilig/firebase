import { jwtSigningKey } from "../config/environment"
import { importPKCS8, KeyLike } from "jose"

let currentJwtSigningKey: KeyLike

// FIXME: Helper until https://github.com/firebase/firebase-functions/pull/1281 is published
const getSecretValue = (name: string) => {
  const val = process.env[name]
  if (val === undefined) {
    console.warn(
      `No value found for secret parameter "${name}". A function can only access a secret if you include the secret in the function's dependency array.`
    )
  }
  return val || ""
}

/**
 * Get the private key used for signing JWTs
 */
export const getCurrentJwtSigningKey = async () => {
  if (!currentJwtSigningKey) {
    console.log("Private key", getSecretValue(jwtSigningKey.name))
    currentJwtSigningKey = await importPKCS8(
      getSecretValue(jwtSigningKey.name),
      "ES256"
    )
  }

  return currentJwtSigningKey
}
