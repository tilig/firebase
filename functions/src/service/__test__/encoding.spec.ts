import { describe, it, expect } from "vitest"
import {
  encodeEncryptedPrivateKey,
  decodeEncryptedPrivateKey,
  encodeSignedPrivateKey,
  decodeSignedPrivateKey,
} from "../encoding"

describe("encoding", () => {
  describe("encodeEncryptedPrivateKey", () => {
    it("encodes into known value", () => {
      expect(
        encodeEncryptedPrivateKey({
          uid: "123",
          email: "e@mail.com",
          encryptedPrivateKey: Buffer.from("ciphertext"),
          issuedAt: 978350400,
        })
      ).toEqual(
        Buffer.from(
          "120a636970686572746578741a03313233220a65406d61696c2e636f6d28c0e2c1d203",
          "hex"
        )
      )
    })
  })

  describe("decodeEncryptedPrivateKey", () => {
    it("decodes into known value", () => {
      const decoded = decodeEncryptedPrivateKey(
        Buffer.from(
          "120a636970686572746578741a03313233220a65406d61696c2e636f6d28c0e2c1d203",
          "hex"
        )
      )

      expect(decoded.uid).toEqual("123")
      expect(decoded.email).toEqual("e@mail.com")
      expect(Number(decoded.issuedAt)).toEqual(978350400)
      expect(decoded.encryptedPrivateKey).toEqual(Buffer.from("ciphertext"))
    })
  })

  describe("encodePlainPrivateKey", () => {
    it("encodes into known value", () => {
      expect(
        encodeSignedPrivateKey({
          encryptedPrivateKey: Buffer.from("message"),
          signature: Buffer.from("signature"),
        })
      ).toEqual(Buffer.from("12076d6573736167651a097369676e6174757265", "hex"))
    })
  })

  describe("decodePlainPrivateKey", () => {
    it("decodes into known value", () => {
      expect(
        decodeSignedPrivateKey(
          Buffer.from("12076d6573736167651a097369676e6174757265", "hex")
        )
      ).toEqual({
        encryptedPrivateKey: Buffer.from("message"),
        signature: Buffer.from("signature"),
      })
    })
  })
})
