import { afterEach, beforeEach, describe, expect, it, vi } from "vitest"
import { encrypt, decrypt, sign, verify } from "../crypto"

import { encryptPrivateKey, decryptPrivateKey } from "../keys"

const reverse = (buf: Buffer) => Buffer.from(buf).reverse()

vi.mock("../crypto", () => ({
  encrypt: vi.fn((plain) => reverse(plain)),
  decrypt: vi.fn((cipher) => reverse(cipher)),
  sign: vi.fn(() => Buffer.from("signature")),
  verify: vi.fn(() => true),
}))

vi.mock("../../config/environment", () => ({
  keyEncryptionKeyName: { value: () => "test-key-asym" },
  signingKeyName: { value: () => "test-key-sign" },
  cryptoClient: {
    getPublicKey: vi.fn(({ name }) =>
      Promise.resolve([
        {
          name: name,
          pem: "public-key-pem",
          pemCrc32c: {
            value: 1912385401,
          },
        },
      ])
    ),
  },
}))

describe("keys", () => {
  beforeEach(() => {
    vi.mocked(encrypt).mockClear()
    vi.mocked(decrypt).mockClear()
    vi.mocked(sign).mockClear()
    vi.mocked(verify).mockClear()
  })

  describe("encryptPrivateKey", () => {
    beforeEach(() => {
      vi.useFakeTimers()
      // Fake timer because we add the current date to the meta data
      const date = new Date(2022, 1, 1, 1, 0, 0)
      vi.setSystemTime(date)
    })

    afterEach(() => {
      vi.useRealTimers()
    })

    it("call encrypt with the proper data", async () => {
      const encryptMock = vi.mocked(encrypt)
      const signMock = vi.mocked(sign)

      const privateKey = Buffer.from("plaintext").toString("base64url")

      await encryptPrivateKey("uid", "e@mail.com", privateKey)

      expect(encryptMock).toHaveBeenCalledOnce()

      expect(encryptMock).toHaveBeenCalledOnce()
      expect(encryptMock).toHaveBeenCalledWith(
        // Zeros, because the private key plaintext buffer is zero-filled.
        // The size should be consistent.
        Buffer.from("plaintext").fill(0),
        "public-key-pem"
      )

      expect(signMock).toHaveBeenCalledOnce()
      expect(signMock).toHaveBeenCalledWith(
        Buffer.from(
          "1209747865746e69616c701a03756964220a65406d61696c2e636f6d28908ce28f06",
          "hex"
        ),
        "test-key-sign"
      )
    })
  })

  describe("decryptPrivateKey", () => {
    it("calls decrypt with the proper data", async () => {
      const decryptMock = vi.mocked(decrypt)
      const verifyMock = vi.mocked(verify)

      await decryptPrivateKey(
        "123",
        "e@mail.com",
        // Valid SignedPrivateKey payload
        Buffer.from(
          "1223120a636970686572746578741a03313233220a65406d61696c2e636f6d28c0e2c1d2031a097369676e6174757265",
          "hex"
        ).toString("base64url")
      )

      expect(decryptMock).toHaveBeenCalledWith(
        Buffer.from("ciphertext"),
        "test-key-asym"
      )
      expect(verifyMock).toBeCalledWith(
        Buffer.from(
          "120a636970686572746578741a03313233220a65406d61696c2e636f6d28c0e2c1d203",
          "hex"
        ),
        Buffer.from("signature"),
        "public-key-pem"
      )
    })
  })
})
