import { beforeEach, describe, expect, it, vi } from "vitest"
import { publicEncrypt } from "node:crypto"
import { crc32c } from "@node-rs/crc32"

import { cryptoClient } from "../../config/environment"
import { decrypt, encrypt, sign, verify } from "../crypto"

interface DecryptionRequest {
  ciphertext: Buffer
  ciphertextCrc32c: {
    value: number
  }
}

const reverse = (buf: Buffer) => Buffer.from(buf).reverse()

const fakeVerifier = {
  update: vi.fn(),
  end: vi.fn(),
  verify: vi.fn(() => true),
}

const fakeHasher = {
  update: vi.fn(),
  digest: vi.fn(() => Buffer.from("signature")),
}

// FIXME: Use real implementation of signing and encrypting?
vi.mock("node:crypto", () => ({
  publicEncrypt: vi.fn((_, plaintext) => reverse(plaintext)),
  createVerify: vi.fn(() => fakeVerifier),
  createHash: vi.fn(() => fakeHasher),
  constants: { RSA_PKCS1_OAEP_PADDING: "padding" },
}))

vi.mock("../../config/environment", () => ({
  keyEncryptionKeyName: "test-key-asym",
  signingKeyName: "test-key-sign",
  cryptoClient: {
    // FIXME: Use real implementation of signing and encrypting?
    asymmetricDecrypt: vi.fn((request: DecryptionRequest) => {
      const decrypted = reverse(request.ciphertext)
      return Promise.resolve([
        {
          plaintext: decrypted,
          plaintextCrc32c: { value: crc32c(decrypted) },
        },
      ])
    }),
    // FIXME: Use real implementation of signing and encrypting?
    asymmetricSign: vi.fn(
      ({ name, digest: { sha256 }, digestCrc32c: { value } }) =>
        Promise.resolve([
          {
            name,
            verifiedDigestCrc32c: true,
            signature: sha256,
            signatureCrc32c: { value },
          },
        ])
    ),
    getPublicKey: vi.fn(({ name }) =>
      Promise.resolve([
        {
          name: name,
          pem: "public-key-pem",
          pemCrc32c: {
            value: 1912385401,
          },
        },
      ])
    ),
  },
}))

describe("crypto", () => {
  describe("encrypt", () => {
    it("performs the encryption request with the proper data", async () => {
      const mock = vi.mocked(publicEncrypt)
      mock.mockClear()

      const buf = Buffer.from("plaintext")
      await encrypt(buf, "public-key-pem")

      expect(mock).toHaveBeenCalledOnce()
      expect(mock).toHaveBeenCalledWith(
        {
          key: "public-key-pem",
          oaepHash: "sha256",
          padding: "padding",
        },
        buf
      )
    })
  })

  describe("decrypt", () => {
    it("performs the encryption request with the proper data", async () => {
      const spy = vi.spyOn(cryptoClient, "asymmetricDecrypt")
      await decrypt(Buffer.from("ciphertext"), "test-key-asym")

      expect(spy).toHaveBeenCalledOnce()
      expect(spy).toHaveBeenCalledWith({
        name: "test-key-asym",
        ciphertext: Buffer.from("ciphertext"),
        ciphertextCrc32c: {
          value: 395617045,
        },
      })
    })

    it("throws if the plaintext checksum does not match", () => {
      const spy = vi.spyOn(cryptoClient, "asymmetricDecrypt")
      spy.mockImplementationOnce(() =>
        Promise.resolve([
          {
            plaintext: Buffer.from("plaintext"),
            plaintextCrc32c: {
              value: 1234,
            },
          },
        ])
      )

      expect(
        decrypt(Buffer.from("ciphertext"), "public-key-pem")
      ).rejects.toThrowError("Plaintext modified in transit")
    })

    it("throws if the plaintext or checksum is missing", () => {
      const spy = vi.spyOn(cryptoClient, "asymmetricDecrypt")
      spy.mockImplementationOnce(() => Promise.resolve([{}]))

      expect(
        decrypt(Buffer.from("ciphertext"), "public-key-pem")
      ).rejects.toThrowError("Cannot validate decryption response from KMS")
    })
  })

  describe("sign", () => {
    it("calls the signing api with proper data", async () => {
      const spy = vi.spyOn(cryptoClient, "asymmetricSign")
      await sign(Buffer.from("message"), "test-key-sign")

      expect(spy).toHaveBeenCalledOnce()
      expect(spy).toHaveBeenCalledWith({
        name: "test-key-sign",
        digest: { sha256: Buffer.from("signature") },
        digestCrc32c: {
          value: 1930420336,
        },
      })
    })

    it("throws when the key name does not match", () => {
      const spy = vi.spyOn(cryptoClient, "asymmetricSign")
      spy.mockImplementationOnce(() => Promise.resolve([{ name: "other" }]))

      expect(
        sign(Buffer.from("message"), "test-key-sign")
      ).rejects.toThrowError("Keyname does not match in sign request")
    })

    it("throws when the it did not verify the crc of the digest", () => {
      const spy = vi.spyOn(cryptoClient, "asymmetricSign")
      spy.mockImplementationOnce(() =>
        Promise.resolve([
          { name: "test-key-sign", verifiedDigestCrc32c: false },
        ])
      )

      expect(
        sign(Buffer.from("message"), "test-key-sign")
      ).rejects.toThrowError("KMS could not verify integrity of message")
    })

    it("throws when there is no integrity data", () => {
      const spy = vi.spyOn(cryptoClient, "asymmetricSign")
      spy.mockImplementationOnce(() =>
        Promise.resolve([{ name: "test-key-sign", verifiedDigestCrc32c: true }])
      )

      expect(
        sign(Buffer.from("message"), "test-key-sign")
      ).rejects.toThrowError("Cannot validate signing response")
    })

    it("throws when the integrity data is not correct", () => {
      const spy = vi.spyOn(cryptoClient, "asymmetricSign")
      spy.mockImplementationOnce(() =>
        Promise.resolve([
          {
            name: "test-key-sign",
            verifiedDigestCrc32c: true,
            signature: Buffer.from("signature"),
            signatureCrc32c: { value: 1234 },
          },
        ])
      )

      expect(
        sign(Buffer.from("message"), "test-key-sign")
      ).rejects.toThrowError("Signing response corrupted in-transit")
    })
  })

  describe("verify", () => {
    beforeEach(() => {
      fakeVerifier.update.mockClear()
      fakeVerifier.verify.mockClear()
    })

    it("performs the verification with the proper data", async () => {
      const buf = Buffer.from("message")
      const sig = Buffer.from("othersignature")

      await verify(buf, sig, "public-signing-key-pem")

      expect(fakeVerifier.update).toHaveBeenCalledOnce()
      expect(fakeVerifier.update).toHaveBeenCalledWith(buf)

      expect(fakeVerifier.verify).toHaveBeenCalledOnce()
      expect(fakeVerifier.verify).toHaveBeenCalledWith(
        {
          key: "public-signing-key-pem",
        },
        sig
      )
    })
  })
})
