import { Buffer } from "node:buffer"
import {
  publicEncrypt,
  constants as CryptoConstants,
  createHash,
  createVerify,
} from "node:crypto"
import { crc32c } from "@node-rs/crc32"

import { cryptoClient } from "../config/environment"

export class EncryptionError implements Error {
  name = "EncryptionError"
  constructor(public message: string) {}
}

/**
 * Asymmetrically decrypt a ciphertext using KMS
 * @param ciphertext Ciphertext
 * @param keyName Key to use for decryption request
 */
export const decrypt = async (ciphertext: Buffer, keyName: string) => {
  const checksum = crc32c(ciphertext)

  const [result] = await cryptoClient.asymmetricDecrypt({
    name: keyName,
    ciphertext,
    ciphertextCrc32c: {
      value: checksum,
    },
  })

  if (
    !result.plaintext ||
    result.plaintextCrc32c?.value === null ||
    result.plaintextCrc32c?.value === undefined
  ) {
    console.error(
      "Cannot validate decryption response from KMS",
      result.plaintext,
      result.plaintextCrc32c?.value
    )
    throw new EncryptionError("Cannot validate decryption response from KMS")
  }

  const plaintext = Buffer.from(result.plaintext)

  if (crc32c(plaintext) !== Number(result.plaintextCrc32c?.value)) {
    throw new EncryptionError("Plaintext modified in transit")
  }

  return plaintext
}

/**
 * Asymmetrically encrypt a buffer with a given public key
 * @param plaintext Plaintext
 * @param publicKeyPem Public key in PEM format
 */
export const encrypt = async (plaintext: Buffer, publicKeyPem: string) => {
  const ciphertext = publicEncrypt(
    {
      key: publicKeyPem,
      oaepHash: "sha256",
      padding: CryptoConstants.RSA_PKCS1_OAEP_PADDING,
    },
    plaintext
  )

  return ciphertext
}

/**
 * Sign the digest of a message using KMS
 * @param message Message to sign
 * @param keyName Name of the key
 */
export const sign = async (message: Buffer, keyName: string) => {
  // Create a digest of the message
  const hash = createHash("sha256")
  hash.update(message)
  const digest = hash.digest()
  const checksum = crc32c(digest)

  const [signResponse] = await cryptoClient.asymmetricSign({
    name: keyName,
    digest: {
      sha256: digest,
    },
    digestCrc32c: {
      value: checksum,
    },
  })

  if (signResponse.name !== keyName) {
    throw new EncryptionError("Keyname does not match in sign request")
  }
  if (!signResponse.verifiedDigestCrc32c) {
    throw new EncryptionError("KMS could not verify integrity of message")
  }
  if (
    !signResponse.signature ||
    signResponse.signatureCrc32c?.value === null ||
    signResponse.signatureCrc32c?.value === undefined
  ) {
    throw new EncryptionError("Cannot validate signing response")
  }

  const signature = Buffer.from(signResponse.signature)

  if (crc32c(signature) !== Number(signResponse.signatureCrc32c.value)) {
    throw new EncryptionError("Signing response corrupted in-transit")
  }

  return signature
}

/**
 * Verify the signature of a message
 * @param message Signed message
 * @param signature Signature of the message
 * @param publicKeyPem Signing Public Key in PEM
 */
export const verify = async (
  message: Buffer,
  signature: Buffer,
  publicKeyPem: string
) => {
  const verify = createVerify("sha256")
  verify.update(message)
  verify.end()

  const verified = verify.verify(
    {
      key: publicKeyPem,
    },
    signature
  )

  if (!verified) {
    console.error("Signature does not match", message, signature)
    throw new EncryptionError("Signature does not match")
  }

  return verified
}
