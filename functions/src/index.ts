import * as functions from "firebase-functions"

import { jwtSigningKey } from "./config/environment"
import { validateAuth } from "./middleware/auth"
import { sessionToken as implSessiontoken } from "./sessionToken"
import { encryptKeypair as implEncryptKeypair } from "./encryptKeypair"
import { decryptKeypair as implDecryptKeypair } from "./decryptKeypair"

/**
 * Handler for creating session tokens
 */
export const getSessionToken = functions
  .runWith({ secrets: [jwtSigningKey] })
  .https.onCall(validateAuth(implSessiontoken))

/**
 * Handler for creating tilig (legacy) tokens
 *
 * This is just copy of getSessionToken, since those
 * JWTs are compatible as far as the backend is concerned.
 */
export const generateTiligToken = getSessionToken

/**
 * Handler for creating JWTs with encrypted keypair
 */
export const encryptKeypair = functions
  .runWith({ secrets: [jwtSigningKey] })
  .https.onCall(validateAuth(implEncryptKeypair))

/**
 * Handler for decrypting private keys
 */
export const decryptPrivateKey = functions.https.onCall(
  validateAuth(implDecryptKeypair)
)
