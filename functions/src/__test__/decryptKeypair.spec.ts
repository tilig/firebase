import { describe, it, expect, vi } from "vitest"

import { VerifiedAuthData } from "../middleware/auth"
import { decryptPrivateKey } from "../service/keys"
import { decryptKeypair, DecryptKeypairRequest } from "../decryptKeypair"

vi.mock("@/service/keys", () => ({
  decryptPrivateKey: vi.fn((...args) => Buffer.from(args[2], "base64url")),
}))

const fakeKey = Buffer.alloc(32)
fakeKey[31] = 1
const fakeKeyBase64 = fakeKey.toString("base64url")

describe("decryptKeypair", () => {
  const request: DecryptKeypairRequest = {
    encrypted_private_key: fakeKeyBase64,
  }
  const auth = {
    uid: "1337",
    token: {
      email: "nonempty@example.com",
      email_verified: true,
    },
  } as VerifiedAuthData

  describe("decrypting", () => {
    it("decrypt the private key", () => {
      expect(decryptKeypair(request, auth)).resolves.toEqual({
        private_key: fakeKeyBase64,
      })
    })
  })

  describe("request parsing", () => {
    it("it calls the parser", () => {
      const spy = vi.spyOn(DecryptKeypairRequest, "safeParse")
      expect(decryptKeypair(request, auth)).resolves.toBeDefined()
      expect(spy).toHaveBeenCalledOnce()
    })

    it("throws on a malformed request", () => {
      const spy = vi.spyOn(DecryptKeypairRequest, "safeParse")
      expect(
        decryptKeypair(
          {
            encrypted_private_key: 1,
          } as unknown as DecryptKeypairRequest,
          auth
        )
      ).rejects.toThrowError("Encryption request is malformed")
      expect(spy).toHaveBeenCalledOnce()
    })
  })

  describe("error handling", () => {
    it("throws if an error is raised during encryption", () => {
      vi.mocked(decryptPrivateKey).mockRejectedValueOnce({
        name: "Error",
        message: "error",
      })
      expect(decryptKeypair(request, auth)).rejects.toThrowError(
        "Cannot decrypt the supplied ciphertext"
      )
    })
  })
})
