import { describe, it, expect, beforeAll, vi } from "vitest"
import { decodeJwt, generateKeyPair, JWTPayload } from "jose"

import { VerifiedAuthData } from "../middleware/auth"
import { sessionToken } from "../sessionToken"

interface SessionTokenPayload extends JWTPayload {
  email: string
  uid: string
  iat: number
  exp: number
}

vi.mock("../service/jwts", () => ({
  getCurrentJwtSigningKey: vi.fn(() =>
    generateKeyPair("ES256").then(({ privateKey }) => privateKey)
  ),
}))

describe("generateToken", () => {
  describe("token generation", () => {
    let token: string
    let decoded: SessionTokenPayload

    beforeAll(async () => {
      const result = await sessionToken(null, {
        uid: "1337",
        token: {
          email: "nonempty@example.com",
          email_verified: true,
        },
      } as VerifiedAuthData)

      token = result.token
      decoded = decodeJwt(token) as SessionTokenPayload
    })

    it("generates a JWT token", () => {
      expect(token).toBeDefined()
    })

    it("includes the proper email and uid", () => {
      expect(decoded.email).toEqual("nonempty@example.com")
      expect(decoded.uid).toEqual("1337")
    })

    it("includes the proper audience", () => {
      expect(decoded.aud).toEqual("tilig-saas")
    })

    it("includes the proper audience", () => {
      expect(decoded.iss).toEqual("tilig-ks")
    })

    it("includes the proper subject", () => {
      expect(decoded.sub).toEqual("1337")
    })

    it("is valid for 60 minutes", () => {
      expect(decoded.exp - decoded.iat).toEqual(3600)
    })
  })
})
