import { describe, it, expect, beforeAll, vi } from "vitest"
import { decodeJwt, generateKeyPair, JWTPayload } from "jose"

import { VerifiedAuthData } from "../middleware/auth"
import {
  encryptKeypair,
  EncryptPrivateKeyRequest,
  KeyInfo,
} from "../encryptKeypair"
import { encryptPrivateKey } from "../service/keys"

vi.mock("../service/keys", () => ({
  encryptPrivateKey: vi.fn((...args) => Buffer.from(args[2], "base64url")),
}))

vi.mock("../service/jwts", () => ({
  getCurrentJwtSigningKey: vi.fn(() =>
    generateKeyPair("ES256").then(({ privateKey }) => privateKey)
  ),
}))

interface EncryptedKeypairToken extends JWTPayload, KeyInfo {
  exp?: number
}

const fakeKey = Buffer.alloc(32)
fakeKey[31] = 1
const fakeKeyBase64 = fakeKey.toString("base64url")

describe("encryptKeypair", () => {
  const request: EncryptPrivateKeyRequest = {
    key: {
      private_key: fakeKeyBase64,
      public_key: fakeKeyBase64,
      key_type: "x25519",
    },
    meta: {
      app_platform: "platform",
      app_version: "version",
    },
  }
  const auth = {
    uid: "1337",
    token: {
      email: "nonempty@example.com",
      email_verified: true,
    },
  } as VerifiedAuthData

  describe("token generation", () => {
    let token: string
    let decoded: EncryptedKeypairToken

    beforeAll(async () => {
      const result = await encryptKeypair(request, auth)
      token = result.token
      decoded = decodeJwt(token) as EncryptedKeypairToken
    })

    it("generates a token", () => {
      expect(token).toBeDefined()
    })

    it("has a proper subject", () => {
      expect(decoded.sub).toEqual(
        "1337#key-7EkW3Sj8TBDXjih8pdnMUe4a5zy_3gjGs3Mky_qsi8U"
      )
    })

    it("has the proper key information", () => {
      expect(decoded.key).toEqual({
        encrypted_private_key: fakeKeyBase64,
        public_key: fakeKeyBase64,
        key_type: "x25519",
      })
    })

    it("has the proper key id", () => {
      expect(decoded.kid).toEqual("7EkW3Sj8TBDXjih8pdnMUe4a5zy_3gjGs3Mky_qsi8U")
    })

    it("has the proper meta data", () => {
      expect(decoded.meta).toEqual({
        app_platform: "platform",
        app_version: "version",
      })
    })

    it("does not expire", () => {
      expect(decoded.exp).toBeUndefined()
    })
  })

  describe("request parsing", () => {
    it("it calls the parser", () => {
      const spy = vi.spyOn(EncryptPrivateKeyRequest, "safeParse")
      expect(encryptKeypair(request, auth)).resolves.toBeDefined()
      expect(spy).toHaveBeenCalledOnce()
    })

    it("throws on a malformed request", () => {
      const spy = vi.spyOn(EncryptPrivateKeyRequest, "safeParse")
      expect(
        encryptKeypair(
          {
            ...request,
            key: {
              ...request.key,
              private_key: Buffer.alloc(33).toString("base64url"),
            },
          },
          auth
        )
      ).rejects.toThrowError("Encryption request is malformed")
      expect(spy).toHaveBeenCalledOnce()
    })
  })

  describe("error handling", () => {
    it("throws if an error is raised during encryption", () => {
      vi.mocked(encryptPrivateKey).mockRejectedValueOnce({
        name: "Error",
        message: "error",
      })
      expect(encryptKeypair(request, auth)).rejects.toThrowError(
        "Error during private key encryption"
      )
    })
  })
})
