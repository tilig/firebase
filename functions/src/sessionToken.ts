import { SignJWT } from "jose"
import { HttpsError } from "firebase-functions/v1/auth"

import { VerifiedAuthData } from "./middleware/auth"
import { getCurrentJwtSigningKey } from "./service/jwts"

/**
 * Claims that will be part of the JWT
 */
interface SessionAttributes extends Partial<Record<string, string>> {
  email: string
  uid: string
}

/**
 * Create a signed JWT with session data
 * @param payload claims to sign
 * @return Signed JWT
 */
const createSessionJwt = async (attributes: SessionAttributes) => {
  try {
    return new SignJWT({ ...attributes, kind: "session-token" })
      .setProtectedHeader({ alg: "ES256" })
      .setAudience("tilig-saas")
      .setIssuer("tilig-ks")
      .setIssuedAt()
      .setExpirationTime("60m")
      .setSubject(attributes.uid)
      .sign(await getCurrentJwtSigningKey())
  } catch (e) {
    console.error("Error signing JWT", e)
    throw new HttpsError(
      "unavailable",
      "Cannot create session tokens at this time"
    )
  }
}

/**
 * Generate a session token
 * @param _ unused data
 * @param context call context
 */
export const sessionToken = async (_: unknown, auth: VerifiedAuthData) => {
  const token = await createSessionJwt({
    email: auth.token.email,
    uid: auth.uid,
  })

  return {
    token,
  }
}
