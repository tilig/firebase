import { KeyManagementServiceClient } from "@google-cloud/kms"
import { defineSecret, defineString } from "firebase-functions/params"

/**
 * JWT signing key
 */
export const jwtSigningKey = defineSecret("JWT_SIGNING_KEY")

/**
 * KMS client
 *
 * Configured to use current service account
 */
export const cryptoClient = new KeyManagementServiceClient()

/**
 * Asymmetric key for keypair encryption (KEK)
 */
export const keyEncryptionKeyName = defineString("KEY_ENCRYPTION_KEY_NAME")

/**
 * Asymmetric key for keypair signing (Signing Key)
 */
export const signingKeyName = defineString("KEY_SIGNING_KEY_NAME")
